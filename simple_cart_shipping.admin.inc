<?php

/**
 * @file
 * Administration pages for Basic Cart promo codes.
 */

/**
 * Form builder. (Menu callback.)
 */
function simple_cart_shipping_admin_form($form, &$form_state) {
  $form = array();

  $form['simple_cart_shipping_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Flat rate shipping amount'),
    '#field_prefix' => '$',
    '#description' => t('This amount will be added to all orders at checkout.'),
    '#default_value' => variable_get('simple_cart_shipping_rate', '25.00'),
    '#size' => 10,
    '#required' => TRUE,
  );
  $form['simple_cart_shipping_free'] = array(
    '#type' => 'checkbox',
    '#title' => t('Offer free shipping on orders over a certain amount.'),
    '#default_value' => variable_get('simple_cart_shipping_free', FALSE),
  );
  $form['simple_cart_shipping_free_min'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum order amount for free shipping.'),
    '#field_prefix' => '$',
    '#description' => t('Orders that total more than this mount will receive free shipping.'),
    '#default_value' => variable_get('simple_cart_shipping_free_min', ''),
    '#size' => 10,
    '#states' => array(
      'visible' => array(
        ':input[name=simple_cart_shipping_free]' => array('checked' => TRUE),
      ),
    ),
  );

  return system_settings_form($form);
}